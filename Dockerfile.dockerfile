#####################################################################################
# serviços oi facade                                                                #
#####################################################################################
FROM tomcat:8.0
MAINTAINER franklin.bourguignon@mobicare.com.br

#####################################################################################
#  configuration                                                                    #
#####################################################################################
ENV server.context-path=/servicosOi

ENV  com.sun.management.jmxremote.port 8100
ENV  com.sun.management.jmxremote.ssl false
ENV  com.sun.management.jmxremote.authenticate false
ENV  java.rmi.server.hostname $HOSTNAME
ENV  user.timezone America/Sao_Paulo
ENV  oi-services-facade.disable.all.offer.persistence true
ENV  LOG_LVL_MOBICARE info
ENV  LOG_LVL_SQL off
ENV  LOG_LVL_SPRING info
ENV  LOG_LVL_ROOT info
ENV  in.servicesEndpoint.consulta https://sdpvas.oi.com.br:8443/eProxy/service/MMPSQ_Port_Router?wsdl
ENV  in.servicesEndpoint.alteracao https://sdpvas.oi.com.br:8443/eProxy/service/OF_Port_Router?wsdl

RUN echo "export JAVA_OPTS=\"\$JAVA_OPTS -Dcom.sun.management.jmxremote.port=8100\"" >> $CATALINA_HOME/setenv.sh && \
    echo "export JAVA_OPTS=\"\$JAVA_OPTS -Dcom.sun.management.jmxremote.ssl=false\"" >> $CATALINA_HOME/setenv.sh && \
    echo "export JAVA_OPTS=\"\$JAVA_OPTS -Dcom.sun.management.jmxremote.authenticate=false\"" >> $CATALINA_HOME/setenv.sh && \
    echo "export JAVA_OPTS=\"\$JAVA_OPTS -Djava.rmi.server.hostname=$HOSTNAME\"" >> $CATALINA_HOME/setenv.sh && \
    echo "export JAVA_OPTS=\"\$JAVA_OPTS -Duser.timezone=America/Sao_Paulo\"" >> $CATALINA_HOME/setenv.sh && \
    echo "export JAVA_OPTS=\"\$JAVA_OPTS -Doi-services-facade.disable.all.offer.persistence=true\"" >> $CATALINA_HOME/setenv.sh && \  
    echo "export JAVA_OPTS=\"\$JAVA_OPTS -DLOG_LVL_MOBICARE=info\"" >> $CATALINA_HOME/setenv.sh && \
    echo "export JAVA_OPTS=\"\$JAVA_OPTS -DLOG_LVL_SQL=off\"" >> $CATALINA_HOME/setenv.sh && \  
    echo "export JAVA_OPTS=\"\$JAVA_OPTS -DLOG_LVL_SPRING=info\""  >> $CATALINA_HOME/setenv.sh && \ 
    echo "export JAVA_OPTS=\"\$JAVA_OPTS -Din.servicesEndpoint.consulta=https://sdpvas.oi.com.br:8443/eProxy/service/MMPSQ_Port_Router?wsdl\"" >> $CATALINA_HOME/setenv.sh && \  
    echo "export JAVA_OPTS=\"\$JAVA_OPTS -Din.servicesEndpoint.alteracao=https://sdpvas.oi.com.br:8443/eProxy/service/OF_Port_Router?wsdl\"" >> $CATALINA_HOME/setenv.sh

WORKDIR /code
ADD ./context.xml $CATALINA_HOME/conf/context.xml
ADD ./mysql-connector-java-5.1.6.jar $CATALINA_HOME/lib/
ADD http://artifactory.alog.mobicare.com.br/artifactory/libs-release-local/com/mobicare/oi-services-facade-rest/1.7.22/oi-services-facade-rest-1.7.22.war $CATALINA_HOME/webapps/servicosOi.war
EXPOSE 8080